//
// Created by juliano on 2021-02-16.
//

#include <iostream>
#include "Human.h"

Human::Human() {
    mAge = 52;
    std::cout << "Constructeur" << std::endl;
}

Human::Human(std::string name) {
    mName = name;
}

Human::Human(std::string name, int age) {
    mAge = age;
    mName = name;
}

Human::~Human() {
    std::cout << "Destructeur de " << mName << std::endl;
}

void Human::manger(int nbr) {
    std::cout << "Nous mangeons " << nbr << " fois par jour!" << std::endl;
}

std::string Human::afficherNom() {
    return mName;
}

int Human::afficherAge() {
    return mAge;
}

void Human::setAge(int age) {
    mAge = age;
}

void Human::setName(std::string nom) {
    mName = nom;
}