#include <iostream>
#include "Human.h"

using namespace std;
int main() {
    // cout << "Hello, World!" << endl;
    Human juliano;
    juliano.setName("Juliano");
    juliano.setAge(20);

    cout << "Je m\'appelle " << juliano.afficherNom() << ". J\'ai " << juliano.afficherAge() << " ans." << endl;

    return 0;
}
