//
// Created by juliano on 2021-02-16.
//

#ifndef PROGOBJET_HUMAN_H
#define PROGOBJET_HUMAN_H


class Human {
    public :

        std::string mCouleurcheveux = "Marron";

        Human();
        Human(std::string name);
        Human(std::string name, int age);

        ~Human();

        void manger(int nbr);

        std::string afficherNom();
        int afficherAge();

        void setName(std::string nom);
        void setAge(int age);

    private:
        int mAge;
        std::string mName;
};


#endif //PROGOBJET_HUMAN_H
